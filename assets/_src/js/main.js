$(function() {

    var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    if (isSafari) {
        $('html').addClass('_safari');
    }

    $('#fullpage').fullpage({
        fixedElements: '#header, #footer',
        anchors: ['intro', 'games', 'offers'],
        menu: '#menu',
        fitToSection: true,
        keyboardScrolling: true,
        animateAnchor: true,
        verticalCentered: false,
        scrollOverflow: true,
        onLeave: function() {
            if ($('.uncover').hasClass('instant')) {
                $('.uncover').removeClass('instant');
            };
            if ($(window).width() < 769 && $('.js-menu').hasClass('opened')) {
                $('.js-menu-trigger').removeClass('active');
                $('.js-menu').removeClass('opened');
                $('.share').removeClass('visible');
                $('.uncover').addClass('instant');
            };
        }
    });

    var gamesSlider = new Swiper('.js-games-slider', {
        slidesPerView: 2.175,
        centeredSlides: true,
        loop: true,
        breakpoints: {
            1440: {
                slidesPerView: 2
            },

            1024: {
                slidesPerView: 1.5
            },
            768: {
                slidesPerView: 1
            }
        }
    });

    if ($(window).width() < 769) {
        $('.menuanchor').on('click', function() {
            $('.js-menu-trigger').removeClass('active');
            $('.js-menu').removeClass('opened');
            $('.share').removeClass('visible');
        })
    }

    gamesSlider.on('resize', function() {
        gamesSlider.update();
    })

    $('.js-game-prev').on('click', function() {
        gamesSlider.slidePrev(1000);
    });
    $('.js-game-next').on('click', function() {
        gamesSlider.slideNext(1000);
    });

    $('.js-menu-trigger').on('click', function() {
        $(this).toggleClass('active');
        $('.js-menu').toggleClass('opened');
        $('.share').toggleClass('visible');
    });

    var rellax = new Rellax('.rellax');
});
